
# Building C apps on Windows in 2021

This document was written on April 3, 2021.  The procedure may change over time.  This is a companion gist to the youtube video here, where I go through every step of both options




[![Youtube Version Of This Document](http://img.youtube.com/vi/r6kM3tR03g4/0.jpg)](https://www.youtube.com/watch?v=r6kM3tR03g4 "Youtube Version Of This Document")



The code for that was written before is here: https://gist.github.com/cnlohr/6e452dc6cc2df7f48d5ade66059358d9

## OPTION 1: TCC / Notepad++, the non-IDE solution

### TCC
 * Visit http://download.savannah.gnu.org/releases/tinycc/ and download `tcc-0.9.27-win64-bin.zip` and `winapi-full-for-0.9.27.zip`
 * NOTE: I place TCC in "tools" but you can place it in any folder
 * Make a new folder, `C:\tools`
 * Unzip `tcc-0.9.27-win64-bin.zip` to `C:\tools`
 * Unzip contents of winapi folder from `winapi-full-for-0.9.27.zip` to `C:\tools\tcc` ontop of existing files, overwriting them
 * Press [Windows]+[Pause] click `Advanced System Settings`
    * Alternatively on the start menu you can search for "advanced system settings" or "environment variables"
 * Click `Environment Variables`
 * Double-click `Path` in `System variables`.  It is usually the bottom option
 * Add `C:\tools\tcc` to your environment path
 * Click OK to the dialogs

### Notepad++
 * Visit https://notepad-plus-plus.org/downloads/
 * Download NP++, at time of writing this, I used https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.9.5/npp.7.9.5.Installer.x64.exe
 * Install NP++
 * Perform previous steps to add `C:\Program Files\Notepad++` to your system path

### Run!
 * Open a command-line terminal by holding [Windows] + R and typing `cmd` and pressing enter. Or by pressing the [Windows] button and typing cmd
 * Type `notepad++ hello.c` into the prompt
 * You want to create a new file when prompted
 * Type the following in

```c
#include <stdio.h>
int main()
{
	printf( "Hello, World!\n" );
	return 0;
}
```
 * Back at terminal, type: `tcc hello.c`
 * Run your application `hello.exe`

### Extra Credit
 * Make `compile.bat` because building inevitably gets complicated
 * `notepad++ compile.bat`
 * Paste `tcc hello.c -o hello.exe` into `compile.bat`
 * From terminal, type `compile` then `hello` to compile and test your application

## OPTION 2: VS Code + GCC

### VS Code
 * Go to the Visual Studio code website: https://code.visualstudio.com/download
 * Download the "64-bit System Installer"
   * At time of writing this, this is the download link: https://code.visualstudio.com/docs/?dv=win64
 * Install VS Code to default paths with default settings

### GCC
 * Visit https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download
 * Being on sourceforge, I do not have a link, but the file name is `mingw-w64-install.exe`
 * Select the following configuration: 8.1.0, x86_64, win32 (Threads), seh (Exceptions), Build 0
 * Install to `C:\tools\mingw...`
 * Add GCC to your environment path
 * Press [Windows]+[Pause] click `Advanced System Settings`
 * Click `Environment Variables`
 * Double-click `Path` in `System variables`
 * Navigate to `C:\tools\{gcc folder}\bin\`
 * Add path including the `\bin` to your environment path
 * Click OK to the dialogs

### Setup your project in VS Code
 * Make a folder on your drive, for instance `C:\projects\hello`
 * Open VS Code
 * File -> Add Folder to Workspace...
 * Select `C:\projects\hello`
   * Alternatively, from a command line prompt say `code hello` from `C:\projects`
 * Create new file by clicking the {files} button on the left
 * Right-cick on the project name on the left (right of the files button)
 * New File -> hello.c
 * Write the hello world application from above in there
 * Add the following two addons, by clicking the {addons} button on the left side
    * C/C++ Intellisense debugging and code browsing, found here: https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools
    * Code Runner, found here: https://marketplace.visualstudio.com/items?itemName=formulahendry.code-runner
 * Click the {run and debug} button on the left hand side
 * Click `Run and Debug`
 * Select `gcc` from the dropdown list
 * BE SURE TO SELECT hello.c again!  You must have the app you want to run up
 * Press [F5] and it will run your program in a debugger
 * You can add breakpoints by pressing on the margin on a given line, and step through your code
 * You may need to modify the build. [Ctrl]+[Shift]+[B] select g++
 * Feel free to edit `tasks.json`

### Hybrid note
 * You can modify your `tasks.json` to execute `compile.bat` then you can arbitrarily make your build system
 * You can access `tasks.json` by holding [Ctrl]+[Shift]+[B] and selecting gcc from the tab
